# NADA Gradle Plugins #

A collection of gradle plugin projects:

* `version` -- string notation example: `nada-gradle-plugins:nada-gradle-version:0.0.7`
* `jpconv` -- string notation example: `nada-gradle-plugins:nada-gradle-jpconv:0.0.7`
* `depx` -- string notation example: `nada-gradle-plugins:nada-gradle-depx:0.0.7`
* `jpamodelgen` string notation example: `nada-gradle-plugins:nada-gradle-jpamodelgen:0.0.7`

These projects are organized as a gradle multi-project with a root project that
facilitates maven local installs and artifactory publishing--if you have access 
to an artifactory instance--see `gradle.properties` for configuration
requirements. These plugins bootstrap themselves and provide a demonstrative 
use case in doing so.

The root project build also supports the gradle wrapper, so you don't need to
worry about downloading gradle, just run `./gradlew` or `gradlw.bat` from the 
root project directory.

Plugins are available in the jcenter artifact repository. The `jpconv` plugin 
depends on `propdeps` plugin, which is only available at `http://repo.spring.io/plugins-release`.
A gradle build script that wants to apply the `version` and `jpconv` plugin would
therefore look something like the following example:

```groovy
buildscript {
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
  dependencies {
    classpath 'org.springframework.build.gradle:propdeps-plugin:0.0.7'
    classpath 'nada-gradle-plugins:nada-gradle-jpconv:0.0.7'
    classpath 'nada-gradle-plugins:nada-gradle-version:0.0.7'
  }
}

project.group = 'some.cool.group'
project.version = '0.0.1-SNAPSHOT'
apply plugin: 'version'
apply plugin: 'jpconv'
```

## nada-gradle-depx ##

[nada-gradle-depx](https://bitbucket.org/troyhart/gradle-plugins/src/6b65143d9334e762b888c70720c2627a0330fa34/depx/README.md?at=master)

A dependency extension plugin which enables build properties to specify arbitrary
dependency version overrides. This facilitates CD processes where you want to 
test your project with a different version of a dependency than what the project
declares, in order to test the viability of the overridden version with your 
project. Additionally, this plugin enables a novel approach to gradle multi-project
configuration which enables a gradle build script to declare dependencies in 
such a way as to allow them to dynamically morph between external and project
level dependencies--which occurs during the build configuration phase.

## nada-gradle-jpconv ##

[nada-gradle-jpconv](https://bitbucket.org/troyhart/gradle-plugins/src/6b65143d9334e762b888c70720c2627a0330fa34/jpconv/README.md?at=master)

A java/groovy project plugin which parses a project's structure and dynamically
applies the `java`, `war`, and `groovy` plugins as appropriate. This plugin
recognizes certain non-standard but historically encountered java project source set 
structures and adapts to them (it recognizes `src/java/main` for example, where
`src/main/java` is standard). This plugin automatically applies the `jacoco` and
`propdeps` plugins and optionally (does by default) apply the `eclipse`, `idea`,
`eclipse-propdeps`, and `idea-propdeps` plugins.

## nada-gradle-version ##

[nada-gradle-version](https://bitbucket.org/troyhart/gradle-plugins/src/6b65143d9334e762b888c70720c2627a0330fa34/version/README.md?at=master)

A plugin that enforces `project.version` is set before the plugin is applied.
The plugin responds to versions ending in `-SNAPSHOT` by replacing the `SNAPSHOT`
suffix with a timestamp and setting the project's status as follows: 
`project.status='integration'`. Non-SNAPSHOT builds will leave the version as 
declared on the project and will set the project's status as follows: 
`project.status='release'`

## nada-gradle-jpamodelgen ##

[nada-gradle-jpamodelgen](https://bitbucket.org/troyhart/gradle-plugins/src/6b65143d9334e762b888c70720c2627a0330fa34/jpamodelgen/README.md?at=master)

A plugin which hasn't been implemented yet. The intent of this plugin is to add
JPA static meta-model source generation which is the key for enabling type safe
JPA criteria query implementations.

## nada-gradle-utils ##

[nada-gradle-utils](https://bitbucket.org/troyhart/gradle-plugins/src/6b65143d9334e762b888c70720c2627a0330fa34/utils/README.md?at=master)

A utility library which implements utilities commonly needed across all the
gradle plugin projects.