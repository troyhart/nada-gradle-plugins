# Dynamic Dependency Extension

`depx`

This plugin extends basic gradle dependency handling with dynamic dependency
handling. It enables a project to declare its dependencies using gradle's 
standard external dependency notations (both the Map and String forms are supported)
but at build time it can actually realize those dependencies as gradle project
dependencies rather than dependency artifacts resolved from external dependency 
repositories.

It probably goes without saying, but this only works in a gradle multi-project
scenario. In such a scenario, the root project must define a `settings.gradle`
that includes some arbitrary set of projects into a multi-project. Any subproject
defined in this way is then a candidate for dynamic dependency substitution, for
any dependencies declared on it from any of the other projects in the multi-project.
The root project in this multi-project must apply the `depx` plugin (either
directly or indirectly through something like the `jpconv` plugin) and then this
root project has the final say in which of these subprojects can be substituted
for a declared external dependency.

Below is an example of a of a root project `settings.gradle` that establishes
a gradle multi-project:

```groovy
rootProject.name = 'nada-foo'

include ':nada-slf4j'
project(':nada-slf4j').projectDir = file("${nada-slf4j-prjDir}")

include ':nada-logback'
project(':nada-logback').projectDir = file("${nada-logback-prjDir}")
```

Below is an example of a root project `build.gradle` that enables all dynamic
substitutions which are realizable:

```groovy
buildscript {
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
  dependencies {
    classpath 'nada-gradle-plugins:nada-gradle-depx:0.0.7'
    classpath 'nada-gradle-plugins:nada-gradle-version:0.0.7'
  }
}
  
defaultTasks 'check'
group = 'com.nada'
project.version = '0.0.1-SNAPSHOT'

apply plugin: 'depx'
apply plugin: 'version'

allprojects {
  apply plugin: 'maven'
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
}
```

Below is an example of a root project `build.gradle` that restricts dynamic
substitution to a specific project:

```groovy
buildscript {
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
  dependencies {
    classpath 'nada-gradle-plugins:nada-gradle-depx:0.0.7'
    classpath 'nada-gradle-plugins:nada-gradle-version:0.0.7'
  }
}
  
defaultTasks 'check'
group = 'com.nada'
project.version = '0.0.1-SNAPSHOT'

apply plugin: 'depx'
apply plugin: 'version'

depx.substitutableLiveProjects.add(project(':nada-logback'))

allprojects {
  apply plugin: 'maven'
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
}
```

Below is an example of a root project `build.gradle` that completely shuts off
dynamic substitution:

```groovy
buildscript {
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
  dependencies {
    classpath 'nada-gradle-plugins:nada-gradle-depx:0.0.7'
    classpath 'nada-gradle-plugins:nada-gradle-version:0.0.7'
  }
}
  
defaultTasks 'check'
group = 'com.nada'
project.version = '0.0.1-SNAPSHOT'

apply plugin: 'depx'
apply plugin: 'version'

depx.substituteAllLiveProjects = false

allprojects {
  apply plugin: 'maven'
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
}
```

While it is true to say the root project is in control, this is not the whole 
story. In order for any project to realize any dynamic dependencies at build
time, it must use the `depx` DSL to declare its dependencies as dynamic. The 
plugin defines an extension (named `depx`) which adds a method
(`dynamicDependency(notation)`) for this purpose. This method supports both the 
String and Map forms of external dependency notations (i.e. '<group>:<name>:<version>'
and [group:'<group>', name:'<name>', version:'<version>'] -- where the angle 
bracketed entries are value place holders). This method will either return the 
given external dependency notation or a gradle subproject dependency.

Below is an example of a build script that applies this plugin and declares 
dynamic dependencies with the `depx` extension:

```groovy
buildscript {
  repositories {
    mavenLocal()
    jcenter()
    maven { url 'http://repo.spring.io/plugins-release' }
  }
  dependencies { classpath 'nada-gradle-plugins:nada-gradle-jpconv:0.0.7' }
}

defaultTasks 'check'
group = 'com.nada'
project.version = '0.0.1-SNAPSHOT'

apply plugin: 'jpconv'

dependencies {
  compile depx.dynamicDependency(group:'com.nada', name:'nada-slf4j', version:'0.0.1-SNAPSHOT')//map notatioin
  compile depx.dynamicDependency('com.nada:nada-slf4j-gson:0.0.1-SNAPSHOT')//string notation...
  compile depx.dynamicDependency('com.nada:nada-utils:0.0.1-SNAOSHOT')
  compile depx.dynamicDependency('ch.qos.logback:logback-classic:1.1.2')
  compile depx.dynamicDependency('org.slf4j:slf4j-api:1.7.7')
}
```

Each invocation of `depx.dynamicDependency(notation)` in the example above will
either return `notation` or `project(':<name>')`. That is, it will return the 
given input (a String or a Map) or a `ProjectDependency` reference. Below I will
summarize the options from the example:

1. `[group:'com.nada', name:'nada-slf4j', version:'0.0.1-SNAPSHOT']` OR `project(':nada-slf4j')`
1. `'com.nada:nada-slf4j-gson:0.0.1-SNAPSHOT'` OR `project(':nada-slf4j-gson')`
1. `'com.nada:nada-utils:0.0.1-SNAOSHOT'` OR `project(':nada-utils')`
1. `'ch.qos.logback:logback-classic:1.1.2'` OR `project(':logback-classic')`
1. `'org.slf4j:slf4j-api:1.7.7'` OR `project(':slf4j-api')`

Notice how this can potentially be used for arbitrary open source projects. I've
shown logback and slf4j, but I'm not saying it actually works for these projects 
specifically. It could, but these code bases would need a gradle build script 
so that it could be included in the multi-project.

Also notice that in the case above, the project is applying the `jpconv` plugin
not the `depx` plugin. Don't be confused by this. The `jpconv` plugin will apply
the `depx` plugin.

## Why do this? 

I originally concieved of this plugin because I was working in an environment
with a very massive code base. There were hundreds of discrete java libraries 
and almost as many applications. Both the libraries and applications expressed 
dependencies on the internal libraries and external ones as necessary. Originally,
internal libraries and applications were explicitly dependent on the HEAD version 
of every other library--it wasn't managed by gradle originally, but it was managed 
in a way very analgous to a massive gradle multi-project where each library 
triggered the build of all dependent internal libraries. I needed a way to enable
each library to evolve independent of every other library. Naturally, this means 
that intra-project dependencies must be declared in terms of a published external 
dependency rather than gradle project level dependencies. However, this approach 
makes it difficult to tool your development environment as you switch between 
projects you are working on. I wanted a way to make it convenient both for 
development and for CD/CI. With this plugin you can you can have the best of 
both worlds.

## Dependency Resolution Strategy

This plugin also applies a dependency resolution strategy against all dependency 
configurations. As such, any transitive dependencies declared  against libraries 
which are included as a subproject will be patched to reference the version
of the subproject. Also, this custom dependency resolution strategy looks at 
other build time properties and enables experimental overrides for any dependency.
Experimental dependency overrides are controlled through build time properties. 
As each dependency is processed by the strategy it checks the build for a property
that matches the name of the external dependency. If gradle finds a property for
this name its value is taken as the version for the identified artifact. For 
example, if you want to override the version of junit across all the projects 
in your multi-project you'd specify the following gradle command line argument: 

* `junit=4.10`

## An Advanced Example

So, hopefully you can see the power of this dynamic dependencies approach. It's
important to recognize that while this approach enables developers to postpone
version configurations maintenance it doesn't remove the need. When code changes
the version of the code must be considered, just as it would need to be considered
when dependency expressions are static. The plugin enables this to be tested
very simply, as dynamic substitutions can be easily turned off (as demonstrated
above) leaving dependency expressions in terms of the static (external) notations.

With that in mind, I want to present below an example of a root project which 
dynamically adds subprojects into a gradle multi-project. The point of this 
is to recognize that non-trivial projects have multiple facets and within certain
contexts (developer 1 working on parts a, b, and c; developer 2 working on parts
c and d; the CD pipeline which is interested in all changing parts...) the need
for dynamic dependencies versus static references changes. 

Root project `settings.gradle`:
```groovy
rootProject.name="nada-clogs"

// This settings attempts to dynamically build a gradle multi-project. Each
// project named in the List<String> argument passed to the dynamicSubprojectIncludes() 
// method identifies one of the potential projects than can be mapped into
// the multi-project. Whether or not any given project is included in the
// multi-project will then depend on whether or not there is a corresponding
// build time property that defines the location of the project's root directory.
// 
dynamicSubprojectIncludes(
    [
      'nada-slf4j',
      'nada-slf4j-gson',
      'nada-logback',
      'nada-utils',
      'nada-floggen'
    ]) { gradlePrjPath, filesystemPath ->
      include gradlePrjPath
      project(gradlePrjPath).projectDir = file(filesystemPath)
    }

def dynamicSubprojectIncludes(List<String> projectName, Closure doInclude) {
  projectName.each { name ->
    // Expected property name is the project name with a '-prjDir' suffix.
    String propName = "${name}-prjDir"
    if (settings.hasProperty(propName)) {
      String filesystemPath = settings.getAt(propName)
      if (filesystemPath) {
        // Here we go! The the build properties defined a mapping to the
        // file system for the named project--include it in the multi-project. 
        doInclude(":$name", filesystemPath)
        // NOTE: gradle project paths are always prefixed with a colon.
      }
    }
  }
}
```

In the example above it's important to remember that the only dependencies that
can be dynamic are the ones declared on other projects (subprojects) included
in the gradle multi-project.

This project also defines the set of project directory properties for the set
of projects which are candidates for dynamic inclusioin into the multi-project.
This documentation allows user's to understand how to setup their environment
to work in the project.

Root project `gradle.properties`:
```properties
# The following properties define the paths to other (standalone) projects
# which this nada-clogs project depends on. Not every dependency is described
# in this way; many are simply 3rd party dependencies which are described in
# terms of group, name, and version (and potentially other qualifiers) which
# which uniquely identify an artifact in an artifact repository. This set of
# projects which require project directory path configurations are a special
# set. This is a set of projects which will evolve with the nada-clogs project.
# In other words, the nada-clogs project may drive requirements for the other
# projects defined in this set. These paths are used in `settings.gradle` for
# the purpose of including the configured projects within a gradle multi-project.
# Failure to specify these properties will only mean that at build time, the
# projects will not be included in the multi-project and therefore the dependency
# will revert to the 3rd party artifact repo resolvable variety.
nada-slf4j-prjDir
nada-slf4j-gson-prjDir
nada-logback-prjDir
nada-utils-prjDir
nada-floggen-prjDir
```

Finally, each user of this build will provide a custom properties files that
defines the filesystem (prjDir) paths appropriate for the context in which 
they are operating.

An example one user's potential custom properties (see: `~/.gradle/gradle.properties`):
```properties
nada-slf4j-prjDir = /home/thart/projects/nada-slf4j
nada-slf4j-gson-prjDir = /home/thart/projects/nada-slf4j-gson
nada-logback-prjDir = /home/thart/projects/nada-logback
nada-utils-prjDir = /home/thart/projects/nada-utils
nada-floggen-prjDir = /home/thart/projects/nada-floggen

```