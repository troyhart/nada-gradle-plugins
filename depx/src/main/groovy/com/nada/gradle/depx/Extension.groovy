package com.nada.gradle.depx

import org.gradle.api.Project

import com.nada.gradle.utils.ProjectUtils

/**
 * An externalized dependencies plugin extension. This extension exposes configuration
 * for substitutable projects. By default, all live projects will be substituted.
 * To disable this, specify the following in your build: 
 * </p>
 * <ul><li>depx.substituteAllLiveProjects = false</li></ul>
 * 
 * <p>
 * To add specific projects for substitution, add the following to your build script,
 * for each selected project:
 * </p>
 * <ul><li>depx.substitutableLiveProjects.add(project(':&lt;selected_project_path&gt;'))</li></ul>
 * 
 * @author troy.hart@gmail.com
 *
 */
class Extension {
  final Project project
  boolean overridesAllowed = false
  boolean substituteAllLiveProjects = true
  Set<Project> substitutableLiveProjects = new HashSet<Project>()

  Extension(final Project project) {
    this.project = project
    // By default, only 'integration' (i.e. SNAPSHOT) builds allow overrides. 
    // This facilitates continuous deliver/integration processes while enforcing
    // sanity when doing a release build--see the version plugin for more info. 
    overridesAllowed = (project.status.equals('integration'))
  }

  /**
   * @param dependencyName the name of an external dependency
   * 
   * @return the substitutable project reference for the named external dependency,
   *  if substitution is enabled for the dependency and a corresponding project
   *  is defined within the multi-project structure, null otherwise.
   */
  public Project getSubstitutable(String dependencyName) {
    Project p = project.findProject(":${dependencyName}")
    if (p && getSubstitutableLiveProjects().contains(p)) {
      project.logger.info "[${project.name}] Substituted specifically: ${dependencyName} -> ${p}"
      return p
    }
    if (isSubstituteAllLiveProjects()) {
      if (p) {
        project.logger.info "[${project.name}] Substituted by default: ${dependencyName} -> ${p}"
      } else {
        project.logger.info "[${project.name}] Substitution allowed, but dependency not a project in multi-project: ${dependencyName}"
      }
      return p
    }
    else {
      if (p) {
        project.logger.info "[${project.name}] Dependency is in the multi-project, but it's not allowed to be substituted: ${dependencyName}"
      } else {
        project.logger.info "[${project.name}] Dependency is not mapped into the multi-project, but even if it were it would not be allowed to be substituted: ${dependencyName}"
      }
      return null
    }
  }

  /**
   * Supports the following string notation: 'group:name:version'.
   * 
   * <p>
   * This implementation does not presently support any modifiers to the 
   * listed notation. This could be supported if needed.
   * 
   * @param externalDependencyStringNotation
   * 
   * @return
   */
  def dynamicDependency(String externalDependencyStringNotation) {
    def dep = externalDependencyStringNotation.split(':')
    dynamicDependency(group:dep[0], name:dep[1], version:dep[2])
  }

  def dynamicDependency(Map externalDependency) {
    Project substitutable = getSubstitutable(externalDependency.name)
    if (substitutable==null) {
      externalDependency
    }
    else {
      project.getDependencies().project(path:":${externalDependency.name}")
    }
  }

  /**
   * @return true if <code>substituteAllLiveProjects</code> is true AND
   * <code>substitutableLiveProjects.isEmpty()</code>, false otherwise.
   */
  public boolean isSubstituteAllLiveProjects() {
    substituteAllLiveProjects && substitutableLiveProjects.isEmpty()
  }

  @Override
  public String toString() {
    "${ProjectPlugin.EXTENSION_NAME}["
    "'project.name':'${project.name}'," +
        "'overridesAllowed':'${overridesAllowed}'," +
        "'substituteAllLiveProjects':'${substituteAllLiveProjects}'," +
        "'substitutableLiveProjects':'${substitutableLiveProjects}']"
  }
}
