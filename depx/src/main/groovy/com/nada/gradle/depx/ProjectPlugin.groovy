package com.nada.gradle.depx

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.DependencyResolveDetails

/**
 * <p>
 * 
 * @author troy.hart@gmail.com
 */
class ProjectPlugin implements Plugin<Project> {

  public static final EXTENSION_NAME = 'depx'

  @Override
  public void apply(Project project) {

    // Extension is only defined on the root project.
    def extension
    if (project.is(project.rootProject)) {
      extension = project.extensions.create EXTENSION_NAME, Extension, project
    } else {
      extension = project.rootProject.extensions.getByName EXTENSION_NAME
    }

    addResolutionStrategy(project, extension)
  }

  /**
   * A resolution strategy which inspects every dependency (first level and all
   * transitive) and selectively overrides any version that is dynamically
   * substituted by {@link Extension#dynamicDependency(java.util.Map)} (or,
   * {@link Extension#dynamicDependency(String)} or which is specified as a 
   * build time property.
   *  
   * <p>
   * Specification of a build property which matches the name of an artifact
   * will cause the value of the property to be substituted for the version
   * of the given artifact. For example, to override the version of say 'junit'
   * to be version 1.0, you could specify the following command line argument
   * when the gradle build is executed:
   * 
   * <ul><li>-Pjunit=1.0</ul>
   *  
   * @param project
   * @param extension
   */
  private void addResolutionStrategy(Project project, Extension extension) {

    project.configurations.all {
      resolutionStrategy {
        eachDependency { DependencyResolveDetails details ->
          project.logger.info "[${project.name}] ${EXTENSION_NAME} dependency configuration resolution strategy processing"
          String requestedDependency = "${details.requested.group}:${details.requested.name}:${details.requested.version}"
          project.logger.debug "[${project.name}] requested dependency: '${requestedDependency}'"
          String overrideProp = details.requested.name
          // overrideProp example: com.nada__nada-util = 3.0.0
          Project substitutedProject = extension.getSubstitutable(details.requested.name)
          if (substitutedProject) {
            // There's an external dependency which in turn has an external dependency declared
            // on a mapped substitutable project...so, the dependency needs to point to the
            // substituted version.
            String substitutedProjectVersion = substitutedProject.version
            if (!substitutedProjectVersion.equals(details.target.version)) {
              project.logger.info "[${project.name}] EXTERNAL DEPENDENCY VERSION OVERRIDE!"
              project.logger.info "[${project.name}] Current request: '${details.requested}'"
              project.logger.info "[${project.name}] Current target: '${details.target}'"
              project.logger.info "[${project.name}] Substituted project version value: '${substitutedProjectVersion}'"
              details.useVersion substitutedProjectVersion
            }
          }
          else {
            if (project.hasProperty(overrideProp)) {
              String overridingVersion = project.getAt(overrideProp)
              if (!overridingVersion.equals(details.target.version)) {//be sure version is changing
                project.logger.info "[${project.name}] EXTERNAL DEPENDENCY VERSION OVERRIDE!"
                project.logger.info "[${project.name}] Current request: '${details.requested}'"
                project.logger.info "[${project.name}] Current target: '${details.target}'"
                project.logger.info "[${project.name}] Overridding value: '${overridingVersion}'"
                details.useVersion overridingVersion
              }
            }
          }

          if (details.requested.version.equals(details.target.version)) {
            project.logger.info "[${project.name}] resolved dependency: '${requestedDependency}'"
          } else {
            project.logger.info "[${project.name}] resolved overridden dependency: '${requestedDependency}' -> '${details.target.version}'"
            if (!extension.overridesAllowed) {
              // When override restrictions are enabled, this case is encountered
              // in two distinct cases. In the first (most direct) case, this happens
              // when an external dependency version override is provided on the
              // command line (with -P<group>__<name>=<version>) or from gradle.properties
              // (<group>__<name>=<version>). In the second case, this happens when
              // you have enabled live project substitution in an adhoc-multi-project
              // and you have two or more mapped projects, and one of the mapped projects
              // expresses a dependency which in turn has a dependency (ie. a transitive
              // dependency) on one of the mapped projects. Your choices to resolve this
              // issue are 3 fold:
              // 1) turn off substitution: rootProject.depx.substituteAllLiveProjects=false
              //    Also, be sure that you don't configure the Project Set "rootProject.depx.substitutableLiveProjects"
              // 2) turn off the override restriction: rootProject.afterEvaluate { rootProject.depx.overridesAllowed=true }
              // 3) map the projects with transitive on other mapped projects into the multi-project--this way, the dependencies will be
              //    substituted during the project's configuration phase and you won't run into any transitive dependencies on your
              //    mapped projects.
              throw new GradleException("[${project.name}:${project.version}] external dependency resolution failure: overrides are restricted for this build; turn off project substitution or the override restriction via ${EXTENSION_NAME} configuration.")
            }
          }
        }
      }
    }
  }
}
