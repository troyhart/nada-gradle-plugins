/**
 * @author troy.hart@gmail.com
 * 
 * This package provides a gradle project plugin and extension for custom
 * dependency processing which enables standalone (friendly) projects
 * to realize project level dependencies at build time as configured by
 * a root adhoc-multi-project.
 */
package com.nada.gradle.depx;