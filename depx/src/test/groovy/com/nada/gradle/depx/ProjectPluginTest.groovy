package com.nada.gradle.depx

import static org.junit.Assert.*

import java.nio.file.Files
import java.nio.file.Path

import org.gradle.api.Project
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.plugins.JavaPlugin
import org.junit.After
import org.junit.Before
import org.junit.Test

import com.nada.gradle.test.utils.ProjectRelativePaths
import com.nada.gradle.test.utils.TestProjectFactory

class ProjectPluginTest {
  Path temp;

  @Before
  void createProjectDir() {
    temp = Files.createTempDirectory("depx-unittest")
  }

  @After
  void cleanupProjectDir() {
    if (!temp.toFile().deleteDir()) {
      throw new IllegalStateException("Couldn't clean up project directory")
    } else {
      println "Cleaned up project directory"
    }
  }

  /**
   * This test ensures that applying the depx plugin doesn't have any side 
   * affects related to the projects' groups, names, versions...
   */
  @Test
  public void verifyNoSideEffectsOnApply() {
    String projectGroup = "com.nada"
    String projectName = "testRootProject"
    String subproject1Name = "testSubproject1"

    Project project = TestProjectFactory.basicProject(temp, projectGroup, projectName, 'test', [
      ProjectRelativePaths.STD_MAIN_JAVA
    ])
    Project subproject1 = TestProjectFactory.basicProject(project, projectGroup, subproject1Name, 'test', [
      ProjectRelativePaths.STD_MAIN_GROOVY
    ])
    project.group = projectGroup
    subproject1.group = projectGroup

    String projectVersion = project.version
    String subproject1Version = subproject1.version

    project.configure(project) {
      allprojects { apply plugin: ProjectPlugin }
    }

    // parent projects are always evaluated before child projects (see org.gradle.api.Project DSL reference)
    project.evaluate()
    subproject1.evaluate()

    assertEquals(projectGroup, project.group)
    assertEquals(projectGroup, subproject1.group)
    assertEquals(projectName, project.name)
    assertEquals(subproject1Name, subproject1.name)
    assertEquals(projectVersion, project.version)
    assertEquals(subproject1Version, subproject1.version)
  }

  /**
   * Verify dynamic dependency behavior of the multi-project configuration given
   * by: {@link #multiprojectWith3SimpleJavaSubprojects(Map, boolean, Closure)(Map, Closure)} when the
   * root project has all substitution turned off.
   */
  @Test
  public void verifyDynamicDependenciesCase1() {

    Project rootProject = multiprojectWith3SimpleJavaSubprojects("1-SNAPSHOT") {
      // Turn off all substitution.
      it.depx.substituteAllLiveProjects = false
    }

    // evaluate
    rootProject.evaluate()
    rootProject.subprojects.each { it.evaluate() }

    // verify
    rootProject.subprojects.each { subprj ->
      subprj.configurations.compile.allDependencies.each { dep ->
        if (!(dep instanceof ExternalModuleDependency)) {
          fail("Expecting an external dependency; got -> $dep")
        }
      }
    }
  }

  /**
   * Verify dynamic dependency behavior of the multi-project configuration given
   * by: {@link #multiprojectWith3SimpleJavaSubprojects(Map, boolean, Closure)} when the
   * root project has all substitution turned on.
   */
  @Test
  public void verifyDynamicDependenciesCase2() {

    Project rootProject = multiprojectWith3SimpleJavaSubprojects("2-SNAPSHOT") {
      // Turn on all substitution.
      it.depx.substituteAllLiveProjects = true
    }

    // evaluate
    rootProject.evaluate()
    rootProject.subprojects.each { it.evaluate() }

    // verify
    rootProject.subprojects.each { subprj ->
      subprj.configurations.compile.allDependencies.each { dep ->
        if (!(dep instanceof ProjectDependency)) {
          fail("Expecting a project dependency; got -> $dep")
        }
      }
    }
  }

  /**
   * Verify dynamic dependency behavior of the multi-project configuration given
   * by: {@link #multiprojectWith3SimpleJavaSubprojects(Map, boolean, Closure)} when the
   * root project has ... TODO finish documentation.
   */
  @Test
  public void verifyDynamicDependenciesCase3() {

    Project rootProject = multiprojectWith3SimpleJavaSubprojects("3-SNAPSHOT") {
      // Specifically enable substitution for subprj1, but no others.
      it.depx.substitutableLiveProjects.add(it.project(":subprj1"))
    }

    // evaluate
    rootProject.evaluate()
    rootProject.subprojects.each { it.evaluate() }

    // verify
    rootProject.subprojects.each { subprj ->
      subprj.configurations.compile.allDependencies.each { dep ->
        if (dep instanceof ProjectDependency) {
          switch (dep.getDependencyProject().getName()) {
            case 'subprj1':
            // expected
              break;
            default:
              fail("Unexpected project dependency -> $dep")
          }
        } else {
          switch (dep.name) {
            case 'subprj2':
            case 'subprj3':
            // expected
              break;
            default:
              fail("Unexpected external module dependency -> $dep")
          }
        }
      }
    }
  }

  /**
   * Verify dynamic dependency behavior of the multi-project configuration given
   * by: {@link #multiprojectWith3SimpleJavaSubprojects(Map, boolean, Closure)} when the
   * root project has ... TODO finish documentation.
   */
  @Test
  public void verifyDynamicDependenciesCase4() {

    Project rootProject = multiprojectWith3SimpleJavaSubprojects("4-SNAPSHOT") {
      // Specifically enable substitution for subprj2, but no others.
      it.depx.substitutableLiveProjects.add(it.project(":subprj2"))
    }

    // evaluate
    rootProject.evaluate()
    rootProject.subprojects.each { it.evaluate() }

    // verify
    rootProject.subprojects.each { subprj ->
      subprj.configurations.compile.allDependencies.each { dep ->
        if (dep instanceof ProjectDependency) {
          switch (dep.getDependencyProject().getName()) {
            case 'subprj2':
            // expected
              break;
            default:
              fail("Unexpected project dependency -> $dep")
          }
        } else {
          switch (dep.name) {
            case 'subprj1':
            case 'subprj3':
            // expected
              break;
            default:
              fail("Unexpected external module dependency -> $dep")
          }
        }
      }
    }
  }

  /**
   * Verify dynamic dependency behavior of the multi-project configuration given
   * by: {@link #multiprojectWith3SimpleJavaSubprojects(Map, boolean, Closure)} when the
   * root project has ... TODO finish documentation.
   */
  //@Test(expected=java.lang.RuntimeException.class)
  // TODO: uncomment the test attribute above to enable this test. I need to
  // figure out how to trigger the resolution strategy in order to test that
  // this method results in an exception when overrides are not allowed, which
  // is switched on by default when the root project is a SNAPSHOT and off by
  // default when the root project is not a SNAPSHOT.
  public void verifyDynamicDependenciesCase5() {

    Project rootProject = multiprojectWith3SimpleJavaSubprojects("5") {
      // Specifically enable substitution for subprj2, but no others.
      it.depx.substitutableLiveProjects.add(it.project(":subprj2"))
    }

    // evaluate
    rootProject.evaluate()
    rootProject.subprojects.each { it.evaluate() }

    // verify
    rootProject.subprojects.each { subprj ->
      subprj.configurations.compile.allDependencies.each { dep ->
        if (dep instanceof ProjectDependency) {
          switch (dep.getDependencyProject().getName()) {
            case 'subprj2':
            // expected
              break;
            default:
              fail("Unexpected project dependency -> $dep")
          }
        } else {
          switch (dep.name) {
            case 'subprj1':
            case 'subprj3':
            // expected
              break;
            default:
              fail("Unexpected external module dependency -> $dep")
          }
        }
      }
    }
  }

  /**
   * Build a multiproject with a simple root and 3 subproject. Configure each
   * subproject as follows:
   * 
   * <ul>
   * <li>subprj1 - no dependencies
   * <li>subprj2 - depends on subprj1
   * <li>subprj3 - depends on both subprj1 and subprj2
   * </ul>
   * 
   * @return the root project.
   */
  public Project multiprojectWith3SimpleJavaSubprojects(rootProjVersion, Closure rootProjectDepxConfiguration) {
    def subprj1 = "subprj1"
    def subprj2 = "subprj2"
    def subprj3 = "subprj3"

    Project rootPrj = TestProjectFactory.multiProject(temp, "com.nada", "testRootPrj", rootProjVersion, [], [
      subprj1,
      subprj2,
      subprj3
    ]) { subprojectName ->
      switch (subprojectName) {
        case subprj1 :
          return [group:'com.nada', version:'0.0.1', extraFolders:[
              ProjectRelativePaths.STD_MAIN_JAVA
            ]]
        case subprj2 :
          return [group:'com.nada', version:'0.0.2', extraFolders:[
              ProjectRelativePaths.STD_MAIN_JAVA
            ]]
        case subprj3 :
          return [group:'com.nada', version:'0.0.3', extraFolders:[
              ProjectRelativePaths.STD_MAIN_JAVA
            ]]
        default :
          throw new RuntimeException("unhandled subproject: $subprojectName")
      }
    }

    rootPrj.configure(rootPrj) {
      allprojects {
        apply plugin: JavaPlugin
        apply plugin: ProjectPlugin
      }

      // invoke closure to configure the depx extension for the root project.
      rootProjectDepxConfiguration(rootPrj)

      Project projectSub1 = project(":$subprj1") //no dependencies here

      Project projectSub2 = project(":$subprj2") {
        dependencies {
          compile depx.dynamicDependency(group:projectSub1.group,
          name:projectSub1.name, version:projectSub1.version)
        }
      }

      project(":$subprj3") {
        dependencies {
          // Here I'm exercising both forms of notation supported by the
          // dynamicDependency method: string notation and map notation.
          compile depx.dynamicDependency(
              "${projectSub1.group}:${projectSub1.name}:${projectSub1.version}")
          compile depx.dynamicDependency(group:projectSub2.group,
          name:projectSub2.name, version:projectSub2.version)
        }
      }
    }
  }
}
