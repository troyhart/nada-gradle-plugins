package com.nada.gradle.jpamodelgen

import org.gradle.api.Project

class Extension {
  final Project project

  Extension(final Project project) {
    this.project = project
  }

  @Override
  public String toString() {
    "${ProjectPlugin.EXTENSION_NAME}["
    "'project.name':'${project.name}']"
  }
}
