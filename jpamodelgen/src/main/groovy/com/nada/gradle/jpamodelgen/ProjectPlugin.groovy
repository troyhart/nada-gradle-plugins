package com.nada.gradle.jpamodelgen

import org.gradle.api.Plugin
import org.gradle.api.Project

class ProjectPlugin
implements Plugin<Project> {
  
  public static final EXTENSION_NAME = 'jpamodelgen'

  @Override
  public void apply(Project project) {

    // *********************************************************
    // setup the jpconv extension
    // *********************************************************
    def extension = project.extensions.create EXTENSION_NAME, Extension, project

  }
}
