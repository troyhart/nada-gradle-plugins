/**
 * @author troy.hart@gmail.com
 * 
 * This package provides a gradle Project Plugin for JPA State Meta-model
 * source code generation.
 *
 */
package com.nada.gradle.jpamodelgen;