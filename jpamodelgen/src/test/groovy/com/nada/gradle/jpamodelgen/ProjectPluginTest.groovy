package com.nada.gradle.jpamodelgen

import static org.junit.Assert.*

import java.nio.file.Files
import java.nio.file.Path

import org.gradle.api.Project
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.plugins.JavaPlugin
import org.junit.After
import org.junit.Before
import org.junit.Test

import com.nada.gradle.test.utils.ProjectRelativePaths
import com.nada.gradle.test.utils.TestProjectFactory

class ProjectPluginTest {
  Path temp;

  @Before
  void createProjectDir() {
    temp = Files.createTempDirectory("jpconv-unittest")
  }

  @After
  void cleanupProjectDir() {
    if (!temp.toFile().deleteDir()) {
      throw new IllegalStateException("Couldn't clean up project directory")
    } else {
      println "Cleaned up project directory"
    }
  }
}
