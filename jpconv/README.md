# Java/JVM Project Conventions

This plugin understands java and jvm project structures, both conventional
and some other non-conventional structures that I've seen over time. As
such, it is able to interrogate your project and determine which of the 
basic java related plugins to apply. It will apply the following:

1. java -- when src/main/java is encountered.
2. war -- when src/main/webapp is encountered.
3. groovy -- when src/main/groovy is encountered.

Additionally, this plugin establishes some some simple rules for versioning
(like it must be specified prior to applying the plugin) and adds logic to 
dynamically append a buildNumber when provided as a build time property.

With this plugin, much of the boilerplate gradle build scripting needed for a
standard java project (standard/enterprise/groovy) is reduced to: `apply plugin: jpconv`

