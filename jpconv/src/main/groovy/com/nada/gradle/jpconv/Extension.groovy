package com.nada.gradle.jpconv

import org.gradle.api.Project

import com.nada.gradle.utils.ProjectUtils;

class Extension {
  final Project project
  boolean skipEclipse
  boolean skipIdea
  boolean compileDebug

  Extension(final Project project) {
    this.project = project
    this.skipEclipse = ProjectUtils.toBoolean(project.properties.skipEclipse, false)
    this.skipIdea = ProjectUtils.toBoolean(project.properties.skipIdea, false)
    this.compileDebug = ProjectUtils.toBoolean(project.properties.compileDebug, false)
  }
  
  @Override
  public String toString() {
    "${ProjectPlugin.EXTENSION_NAME}["
    "'project.name':'${project.name}'," +
    "'compileDebug':'${compileDebug}'," +
    "'skipEclipse':'${skipEclipse}'," +
    "'skipIdea':'${skipIdea}']"
  }
}
