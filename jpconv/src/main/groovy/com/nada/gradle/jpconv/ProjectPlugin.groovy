package com.nada.gradle.jpconv

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.bundling.Jar


/**
 * <p>
 * This plugin analyzes the directory structure of your project and applies the 
 * standard java, war and groovy plugins based on the existence of src/main/java, 
 * src/main/webapp and/or src/main/groovy respectively. This plugin also attempts
 * to support legacy java application directory structures. Specifically, if it
 * detects src/java/main then it will configure sourceSets.main.java.srcDirs
 * accordingly. In this legacy case it will also appropriately set
 * sourceSets.main.resources.srcDirs, sourceSets.test.java.srcDirs and 
 * sourceSets.test.resources.srcDirs.
 * <p>
 * 
 * <p>
 * In addition to the java project specific plugins applied by this implementation
 * there are also a couple others very useful plugins applied, incluing:
 * jacoco and propdeps. It conditionally applies: propdeps-eclipse and propdeps-idea. 
 * It does a variety of other conventional things as well and is responsive to 
 * build script configuration through plugin extensions. See the source for more 
 * details.
 * </p>
 * 
 * @author troy.hart@gmail.com
 *
 */
class ProjectPlugin implements Plugin<Project> {
  public static final EXTENSION_NAME = 'jpconv'

  @Override
  public void apply(Project project) {

    def extension = project.extensions.create EXTENSION_NAME, Extension, project

    addJavaPluginCallbackForArchiveTasksAndTestsConfiguration(project)
    applyJavaPluginIfJavaStructure(project)
    applyWarPluginIfWarStructure(project)
    applyGroovyPluginIfGroovyStructure(project)
    project.apply plugin: 'jacoco'
    project.apply plugin: 'propdeps'
    applyEclipsePluginUnlessFlagged(project, extension)
    applyIdeaPluginUnlessFlagged(project, extension)
    addCompilerDebugSettingsIfEnabled(project, extension)
  }

  private void addCompilerDebugSettingsIfEnabled(Project project, Extension extension) {
    project.configure(project) {
      afterEvaluate {
        if (extension.isCompileDebug()) {
          compileJava {
            options.debug = true
            options.debugOptions.debugLevel = 'source,lines,vars'
          }
        }
        project.logger.info "[${project.name}] compileJava.options.debug = ${compileJava.options.debug}"
        project.logger.info "[${project.name}] compileJava.options.debugOptions.debugLevel = ${compileJava.options.debugOptions.debugLevel}"
      }
    }
  }

  private void applyIdeaPluginUnlessFlagged(Project project, Extension extension) {
    project.configure(project) {
      if (!extension.skipIdea) {
        apply plugin: 'idea'
        apply plugin: 'propdeps-idea'
      }
    }
  }

  private void applyEclipsePluginUnlessFlagged(Project project, Extension extension) {
    project.configure(project) {
      if (!extension.skipEclipse) {
        apply plugin: 'eclipse'
        apply plugin: 'propdeps-eclipse'
        eclipse {
          classpath {
            downloadSources=true
            downloadJavadoc=true
            defaultOutputDir = file('build-eclipse')
          }
          // TODO: fix the link below
          //          project {
          //            linkedResource name: 'gradle-user.properties', type: '1', location: "${System.getProperty('user.home')}/.gradle/gradle.properties"
          //          }
        }
        // always get rid of the settings, when they linger they make the eclipse
        // experience a little buggy.
        task('cleanEclipseSettings', type: Delete) { delete '.settings' }
        cleanEclipse.dependsOn cleanEclipseSettings
      }
    }
  }

  /**
   * 
   * @param project
   */
  private void applyJavaPluginIfJavaStructure(Project project) {
    project.configure(project) {
      if (file('src/main/java').exists()) {
        apply plugin: 'java'
      } else if (file('src/java/main').exists()) {
        // attempt account for old (bad/broken) conventions
        apply plugin: 'java'
        sourceSets.main.java.srcDirs = ['src/java/main']
        if (file('src/java/main/resources').exists()) {
          sourceSets.main.resources.srcDirs = ['src/java/main/resources']
        }
        if (file('src/java/test').exists()) {
          sourceSets.test.java.srcDirs = ['src/java/test']
        }
        if (file('src/java/test/resources').exists()) {
          sourceSets.test.resources.srcDirs = ['src/java/test/resources']
        }
      }
    }
  }

  /**
   * 
   * @param project
   */
  private void applyWarPluginIfWarStructure(Project project) {
    project.configure(project) {
      if (file('src/main/webapp').exists()) {
        apply plugin: 'war'
        // Web applications with non-standard structures must apply the war plugin
        // and configure it as appropriate. Refer to war plugin docs.
      }
    }
  }

  /**
   * 
   * @param project
   */
  private void applyGroovyPluginIfGroovyStructure(Project project) {
    project.configure(project) {
      if (file('src/main/groovy').exists()) {
        apply plugin: 'groovy'
        // going to assume that we don't have legacy groovy code where we need to support
        // old (bad/broken) conventions.
      }
    }
  }

  /**
   * Add a callback to fire when the JavaPlugin is applied. This plugin
   * will ...
   * 
   * @param project
   */
  private void addJavaPluginCallbackForArchiveTasksAndTestsConfiguration(Project project) {

    project.plugins.withType(JavaPlugin) {
      project.configure(project) {
        configurations { tests }

        task('jarTest', type: Jar) {
          dependsOn testClasses
          classifier 'tests'
          from sourceSets.test.output
        }

        task('jarSources', type: Jar) {
          dependsOn classes
          classifier 'sources'
          from sourceSets.main.allSource
        }
        // TODO: javadoc process is failing. It's not critical for now so let's revisit this later.
        //        task('jarJavadocs', type: Jar) {
        //          dependsOn javadoc
        //          classifier 'javadoc'
        //          from javadoc.destinationDir
        //        }

        artifacts {
          tests jarTest
          archives jarTest
          archives jarSources
          //archives jarJavadocs
        }
      }
    }
  }
}
