package com.nada.gradle.jpconv

import static org.junit.Assert.*

import java.nio.file.Files
import java.nio.file.Path

import org.gradle.api.Project
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.plugins.JavaPlugin
import org.junit.After
import org.junit.Before
import org.junit.Test

import com.nada.gradle.test.utils.ProjectRelativePaths
import com.nada.gradle.test.utils.TestProjectFactory

class ProjectPluginTest {
  Path temp;

  @Before
  void createProjectDir() {
    temp = Files.createTempDirectory("jpconv-unittest")
  }

  @After
  void cleanupProjectDir() {
    if (!temp.toFile().deleteDir()) {
      throw new IllegalStateException("Couldn't clean up project directory")
    } else {
      println "Cleaned up project directory"
    }
  }
  
  @Test
  void testNonJavaProjectStructure()
  {
    // Verify that the java plugin is not applied when a java project structure 
    // (standard or recognized non-standard) is not encountered.
    
    // TODO: implement
  }
  
  @Test
  void testStandardJavaProject()
  {
    // Verify that the java plugin is applied when standard java project 
    // structure is encountered
    
    // TODO: implement
  }
  
  @Test
  void testNonstandardJavaProject()
  {
    // Verify that the java plugin is applied when recognized non-standard java 
    // project structure is encountered
    
    // TODO: implement
  }
  
  @Test
  void testStandardGroovyProject()
  {
    // Verify that the groovy plugin is applied when standard groovy project 
    // structure is encountered
    
    // TODO: implement
  }
  
  @Test
  void testStandardWebappProject()
  {
    // Verify that the war plugin is applied when standard war project 
    // structure is encountered
    
    // TODO: implement
  }
}
