package com.nada.gradle.test.utils;

public class ProjectRelativePaths
{
  public static final String STD_MAIN_JAVA = "src/main/java";
  public static final String NONSTD_MAIN_JAVA = "src/java/main";

  public static final String STD_MAIN_GROOVY = "src/main/groovy";

  public static final String STD_MAIN_WEBAPP = "src/main/webapp";

  public static final String STD_TEST_JAVA = "src/test/java";
  public static final String NONSTD_TEST_JAVA = "src/java/test";

  public static final String STD_TEST_GROOVY = "src/test/groovy";

  // TODO: determine if this would be used...seems like this 
  // would really represent an integration test, not a unit test.
  //public static final String STD_TEST_WEBAPP = "src/test/webapp";
}
