package com.nada.gradle.test.utils

import java.nio.file.Files
import java.nio.file.Path

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.After;

public class TestProjectFactory {

  static final String PROJECT_GROUP = "group"
  static final String PROJECT_NAME = "name"
  static final String PROJECT_VERSION = "version"
  static final String PROJECT_DIR = "projectDir"
  static final String PARENT_PROJECT = "parentProject"
  static final String EXTRA_FOLDERS = "extraFolders"

  /**
   * @param projectDir that project's root directory
   * @param group the project's group
   * @param name the name of the project
   * @param version the project's version
   * @param extraFolders all the additional folders that need to be created--paths are relative to projectDir.
   * 
   * @return a basic gradle project to use as a test fixture.
   */
  static final Project basicProject(Path projectDir, String group, String name, String version, List<String> extraFolders) {
    Project prj = basicProjectBuilder(name, projectDir, extraFolders).build()
    prj.group = group
    prj.version = version
    prj
  }

  /**
   * @param parentProject the new project's parent project.
   * @param group the project's group
   * @param name the name of the project
   * @param version the project's version
   * @param extraFolders all the additional folders that need to be created--paths are relative to parentProject.projectDir/name.
   * 
   * @return a basic gradle subproject to use as a test fixture.
   */
  static final Project basicProject(Project parentProject, String group, String name, String version, List<String> extraFolders) {
    File projectDir = parentProject.getProjectDir()
    Path subprojectDir = Files.createDirectories(projectDir.toPath().resolve(name))
    Project prj = basicProjectBuilder(name, subprojectDir, extraFolders).withParent(parentProject).build()
    prj.version = version
    prj.group = group
    prj
  }

  /**
   * Provide all the required parameters in a map. See parameter names defined
   * on this class (eg. {@link #PARENT_PROJECT}). Parameters must be consistent
   * with one of the other <code>basicProject*</code> methods: 
   * {@link #basicProject(Path, String, String, String, List)} or
   * {@link #basicProject(Project, String, String, String, List)}.
   * 
   * @param params a map of required parameters.
   * 
   * @return a basic gradle project to use as a test fixture--it may be a root 
   * project or a subproject.
   */
  public static final Project basicProject(Map params) {
    if (params.get(PARENT_PROJECT)) {
      basicProject(params.get(PARENT_PROJECT), params.get(PROJECT_GROUP), params.get(PROJECT_NAME), params.get(PROJECT_VERSION), params.get(EXTRA_FOLDERS))
    } else {
      basicProject(params.get(PROJECT_DIR),    params.get(PROJECT_GROUP), params.get(PROJECT_NAME), params.get(PROJECT_VERSION), params.get(EXTRA_FOLDERS))
    }
  }

  public static final Project multiProject(Path projectDir, String projectGroup, String projectName,
      String version, List<String> extraFolders, List<String> subprojectNames,
      Closure buildSubprojectConstructorArguments) {
      
    Project project = basicProject(projectDir, projectGroup, projectName, version, [])
    subprojectNames.each {
      Map args = buildSubprojectConstructorArguments(it)
      args.put(PARENT_PROJECT, project)
      args.put(PROJECT_NAME, it)
      Project subproject = basicProject(args)
    }
    
    project
  }

  private static final ProjectBuilder basicProjectBuilder(String name, Path projectDir, List<String> extraFolders) {
    extraFolders.each {
      Files.createDirectories(projectDir.resolve(it))
    }
    return ProjectBuilder.builder().withName(name).withProjectDir(projectDir.toFile())
  }
}
