package com.nada.gradle.utils;

import java.io.File;
import java.util.Date;

import org.gradle.api.Project;

public class FileUtils
{
  public static final void listDirectory(Integer level, File directory, Project project)
  {
    File[] files = directory.listFiles();
    for (int f = 0; f < files.length; f++) {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < level; i++) {
        sb.append("    ");
      }
      if (f < files.length - 1) {
        sb.append("├── ");
      }
      else {
        sb.append("└── ");
      }
      sb.append(files[f].getName());
      if (files[f].isDirectory()) {
        project.getLogger().quiet(String.format("%s [D --> count: %s; modified: %s]", sb.toString(), files[f].listFiles().length, new Date(
            files[f].lastModified())));
        listDirectory(level + 1, files[f], project);
      }
      else {
        project.getLogger().quiet(String.format("%s [F --> size: %s; modified: %s]", sb.toString(), files[f].length(),
            new Date(files[f].lastModified())));
      }
    }
  }
}
