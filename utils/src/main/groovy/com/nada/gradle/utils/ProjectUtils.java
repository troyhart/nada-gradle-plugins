package com.nada.gradle.utils;

import org.gradle.api.Project;

public class ProjectUtils
{
  public static final String SNAPSHOT = "SNAPSHOT";

  public static final String name(Project project)
  {
    return ((project == null) ? "Project[NULL]" : String.format("Project[group='%s'; name='%s'; version='%s']",
        project.getGroup(), project.getName(), project.getVersion()));
  }

  public static final boolean toBoolean(String val, boolean defaultValue) {
    if (val==null || val.isEmpty()) {
      return defaultValue;
    }
    if (val.equalsIgnoreCase("true")||val.equalsIgnoreCase("yes")||val.equalsIgnoreCase("on")||val.equals("1")) {
      return true;
    } else if (val.equalsIgnoreCase("false")||val.equalsIgnoreCase("no")||val.equalsIgnoreCase("off")||val.equals("0")) {
      return false;
    }
    return defaultValue;
  }
}
