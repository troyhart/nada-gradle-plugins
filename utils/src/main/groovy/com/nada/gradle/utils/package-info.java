/**
 * @author troy.hart@gmail.com
 * 
 * This package provides some common gradle utilities.
 *
 */
package com.nada.gradle.utils;