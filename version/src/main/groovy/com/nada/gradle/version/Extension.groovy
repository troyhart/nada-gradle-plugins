package com.nada.gradle.version

import org.gradle.api.Project

import com.nada.gradle.utils.ProjectUtils;

/**
 * @author troy.hart@gmail.com
 *
 */
class Extension {
  final Project project

  Extension(final Project project) {
    this.project = project
  }

  @Override
  public String toString()
  {
    "${ProjectPlugin.EXTENSION_NAME}['project.name':'${project.name}']"
  }
}
