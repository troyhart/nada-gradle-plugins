package com.nada.gradle.version

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.DependencyResolveDetails
import org.gradle.api.artifacts.dsl.DependencyHandler

import com.nada.gradle.utils.ProjectUtils;

/**
 * This plugin insists that the <code>project.version</code> is not the default
 * value, throwing an exception with it finds it to be. The next thing it does
 * is transform <code>project.version</code> into a {@link Version} instance.
 * This transformation turns a changing module (SNAPSHOT) build into timestamped
 * static module build, by removing the 'SNAPSHOT' suffix.
 * 
 * <p>
 * This plugin also set's the project's status to 'release' or 'integration'. 
 * By default, after applying this plugin <code>project.status</code> will
 * be 'release'. However, when a SNAPSHOT version is encountered 
 * <code>project.status</code> will be set to 'integration'.
 * 
 * <p>
 * Consider the following example.
 * 
 * <pre>
 * project.version = '1.0-SNAPSHOT'
 * apply plugin: 'version'
 * println "['project.status': '${project.status}', 'project.version': '${project.version}']"
 * </pre>
 * 
 * The output from the <code>println</code> statement above will be
 * 
 * <pre>
 * ['project.status': 'integration', 'project.version': '1.0-timestamp']
 * </pre>
 * -- where timestamp above will be an actual timestamp value.
 * 
 * @author troy.hart@gmail.com
 */
class ProjectPlugin implements Plugin<Project> {

  public static final EXTENSION_NAME = 'version'

  @Override
  public void apply(Project project) {

    def extension = project.extensions.create EXTENSION_NAME, Extension, project
    
    // *********************************************************
    // Version specification MUST happen before this plugin
    // is applied! Fail if not specified.
    // *********************************************************
    if (project.version == org.gradle.api.Project.DEFAULT_VERSION) {
      throw new GradleException('Must specify \'project.version\' before applying this plugin.')
    }
    
    project.version = new Version(project.version)
    project.status = project.version.release ? 'release' : 'integration'
  }
}