package com.nada.gradle.version

import java.text.SimpleDateFormat

class Version {
  String originalVersion
  String thisVersion
  boolean release = true
  Date buildTime

  Version(String versionValue) {
    buildTime = new Date()
    originalVersion = versionValue
    if (originalVersion.endsWith('-SNAPSHOT')) {
      release = false
      thisVersion = (originalVersion - 'SNAPSHOT') + getTimestamp()
    } else {
      thisVersion = versionValue
    }
  }

  String getTimestamp() {
    // Convert local file timestamp to UTC
    def sdf = new SimpleDateFormat('yyyyMMddHHmmss')
    sdf.setCalendar(Calendar.getInstance(TimeZone.getTimeZone('UTC')));
    return sdf.format(buildTime)
  }

  String toString() {
    thisVersion
  }
}
