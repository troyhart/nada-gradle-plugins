/**
 * @author troy.hart@gmail.com
 * 
 * This package provides a gradle project plugin and extension for custom
 * version processing.
 */
package com.nada.gradle.version;