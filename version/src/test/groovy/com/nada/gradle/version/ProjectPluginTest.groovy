package com.nada.gradle.version

import static org.junit.Assert.*

import java.nio.file.Files
import java.nio.file.Path

import org.gradle.api.Project
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.plugins.JavaPlugin
import org.junit.After
import org.junit.Before
import org.junit.Test

import com.nada.gradle.test.utils.ProjectRelativePaths
import com.nada.gradle.test.utils.TestProjectFactory

class ProjectPluginTest {
  Path temp;

  @Before
  void createProjectDir() {
    temp = Files.createTempDirectory("version-unittest")
  }

  @After
  void cleanupProjectDir() {
    if (!temp.toFile().deleteDir()) {
      throw new IllegalStateException("Couldn't clean up project directory")
    } else {
      println "Cleaned up project directory"
    }
  }
  
  @Test
  void testSNAPSHOTVersion()
  {
    // Verify that SNAPSHOT versions yield project.status = 'integration'
    // and that the build script configurred project.version (string value)
    // is transformed to replace the SNAPSHOT suffix with a timestamp.
  }
  
  @Test
  void testNonSNAPSHOTVersion()
  {
    // Verify that Non-SNAPSHOT versions yield project.status = 'release'
    // and that the build script configurred project.version (string value)
    // is unchanged from configuration. 
  }
}
